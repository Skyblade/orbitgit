﻿using UnityEngine;
using System.Collections;

public class EnemyContoller : MonoBehaviour {

    public Transform target;
    public float Speed = 1f;
    public int Health = 100;
    public Color HealthyColor;
    public Color DeathColor;
    public int Reward;
    public float Damage = 5f;

    private SpriteRenderer spriteRenderer;
    private HeroController heroController;
    private EnemySpawner enemySpawner;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        heroController = target.gameObject.GetComponent<HeroController>();
	}

    public void Initialize(Transform target, EnemySpawner enemySpawner) 
    {
        this.target = target;
        this.enemySpawner = enemySpawner;
    
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 direction =enemySpawner.GetDirection(transform.position);
        transform.position += direction * Speed * Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D other) 
	{
		if (other.gameObject.tag == "Weapon") 
		{
			Debug.Log("Hit");
			
			Health -= 22;
			
			spriteRenderer.color = Color.Lerp(DeathColor, HealthyColor, Mathf.Clamp(Health / 100f, 0f, 1f));
			if (Health <= 0) 
			{
				enemySpawner.aliveEnemies.Remove(this.transform);
				heroController.AddScore(Reward);
				
				if (enemySpawner.aliveEnemies.Count == 0) 
				{
					Application.LoadLevel(Application.loadedLevel);
					/*if (Application.loadedLevel < 3)
					{
						Application.LoadLevel(Application.loadedLevel + 1);
					}
					else 
					{
						Application.LoadLevel(Application.loadedLevel);
					}*/
				}
				Destroy(gameObject);
			}
		}
		else if (other.gameObject.tag == "Player") 
		{
			heroController.ApplyDamage(Damage);
		}
		
	}


	void OnCollisionStay2D(Collision2D other)
    {
		if (other.gameObject.tag == "Player")
        {
            heroController.ApplyDamage(Damage*Time.fixedDeltaTime);

        }
    }
}
