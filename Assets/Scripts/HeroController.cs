﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeroController : MonoBehaviour
{
    public float LeftMotorSpeed = 100f;
    public float RightMotorSpeed = 100f;

    public HingeJoint2D LeftJoint;
    public HingeJoint2D RightJoint;

    
    public int scores = 0;
    public Text ScoreText;
    public Color HealthyColor;
    public Color DeathColor;

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D leftWeaponRigidbody;
    private Rigidbody2D rightWeaponRigidbody;
    private float health = 100;
	private float pinnedMotorTorque = 40f;
	private float unPinnedMotorTorque = 10f;
    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        Debug.Assert(LeftJoint, "Left joint is not assigned");
        Debug.Assert(RightJoint, "Right joint is not assigned");

        leftWeaponRigidbody = LeftJoint.connectedBody;
        rightWeaponRigidbody = RightJoint.connectedBody;
    }

    // Update is called once per frame
    void Update()
    {
		JointMotor2D leftMotor = LeftJoint.motor;
		JointMotor2D rightMotor = RightJoint.motor;

		/*float leftStick = Input.GetAxisRaw("Vertical");
        leftMotor.motorSpeed = -leftStick * LeftMotorSpeed;
        LeftJoint.motor = leftMotor;
*/
		float rightStick = Input.GetAxisRaw("Weapon");
        rightMotor.motorSpeed = -rightStick * RightMotorSpeed;
        RightJoint.motor = rightMotor;

        if (Input.GetButtonDown("LeftPin"))
        {
			rightWeaponRigidbody.constraints = RigidbodyConstraints2D.FreezePosition;
            rightMotor = RightJoint.motor;
			rightMotor.maxMotorTorque = pinnedMotorTorque;
			rightMotor.motorSpeed -= LeftMotorSpeed * rightStick;
            RightJoint.motor = rightMotor;
        }
		else if (Input.GetButtonUp("LeftPin"))
        {
			rightMotor = RightJoint.motor;
			rightMotor.maxMotorTorque = unPinnedMotorTorque;
			RightJoint.motor = rightMotor;
			rightWeaponRigidbody.constraints = RigidbodyConstraints2D.None;
        }

       /* if (Input.GetButton("RightPin"))
        {
            rightWeaponRigidbody.constraints = RigidbodyConstraints2D.FreezePosition;
            leftMotor = LeftJoint.motor;
            leftMotor.motorSpeed -= RightMotorSpeed * rightStick;
            LeftJoint.motor = leftMotor;
        }
        else
        {
            rightWeaponRigidbody.constraints = RigidbodyConstraints2D.None;
        }*/
    }

    public void AddScore(int amount)
    {
        scores += amount;
        ScoreText.text = scores.ToString();
    }

    public void ApplyDamage(float damage) 
    {
        health -= damage;
        spriteRenderer.color = Color.Lerp(DeathColor, HealthyColor, Mathf.Clamp(health / 100f, 0f, 1f));
        if (health <= 0)
            Application.LoadLevel(Application.loadedLevel);
    }
}
