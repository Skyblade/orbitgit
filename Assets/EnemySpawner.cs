﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EnemySpawner : MonoBehaviour
{
    public GameObject prefab;
    public List<Transform> aliveEnemies = new List<Transform>();
    public Transform HeroTranform;

    // Use this for initialization
    void Start()
    {
        aliveEnemies = GameObject.FindGameObjectsWithTag("Enemy").Select(g => g.transform).ToList();

        foreach (Transform enemy in aliveEnemies) 
        {
            enemy.GetComponent<EnemyContoller>().Initialize(HeroTranform, this);
        }
    }

    public Vector3 GetDirection(Vector3 position) 
    {
        Vector2 result = Vector2.zero;


        Vector3 enemyVector = ( HeroTranform.position-position).normalized * 2;
        result += new Vector2(enemyVector.x, enemyVector.y);

        foreach (Transform other in aliveEnemies)
        {
            if (other.position != position)
            {
                enemyVector = (position - other.position).normalized * 1 / Vector3.Distance(other.position, position) * 1f;
                result += new Vector2(enemyVector.x, enemyVector.y);
            }
        }

        return result.magnitude > 0.1f ? result.normalized : result;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
